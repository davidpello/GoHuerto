package main

import (
	"github.com/gorilla/mux"
)

func MakeRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/data/{node}/{field}/{period}", DataHandler)
	r.HandleFunc("/last/{node}", LastHandler)
	r.HandleFunc("/nodes", NodesHandler)
	r.HandleFunc("/charts", ChartsHandler)
	r.HandleFunc("/upload", UploadHandler).Methods("POST")
	return r
}

