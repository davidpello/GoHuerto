
function gen_chart(dataset, target, name, unit, ymax, ymin, color) {
    // get labels and values
    var labels = [];
    var tempdata = [];
    for(d of dataset) {
	tempdata.push(d["data"])
	// parse dates
	var dt = new Date(Date.parse(d["date"]));
	const options = { hour: '2-digit',  minute: '2-digit', month: 'numeric', day: 'numeric' };
	labels.push(dt.toLocaleDateString("es-ES", options));
    }    

    // configure chart
    const ctx = document.getElementById(target).getContext('2d');
    const config = {
	responsive: true,
	plugins: {
            legend: {
		position: 'top',
		labels: {
		    color: '#dddddddd',
		    font: {
			weight: "bold",
		    },
		},
            },
            title: {
		display: false,
		text: 'Data chart'
            }
	},
	scales: {
	    y: {
		scaleBeginAtZero : false,
		scaleOverride: true,
		scaleStartValue: ymin,
		min: ymin,
		max: ymax,
		ticks: {
		    display: true,
		    autoSkip: false,
		    callback: function(value, index, values) {
			return value + unit;
		    },
		    color: '#dddddddd',
		},
		grid: {
		    color: "#2c323566",
		},
	    },
	    x: {
		ticks: {
		    color: "#dddddddd",
		},
		grid: {
		    color: "#2c323566",
		},
	    }
	}
    };

    const data = {
	labels: labels,
	datasets: [
	    {
		label: name,
		data: tempdata,
		fill: true,
		cubicInterpolationMode: 'monotone',
		borderColor: color + "cc", 
		backgroundColor: color + "22",
		pointStyle: 'circle',
		pointRadius: 0,
		pointHitRadius: 10,
		pointHoverRadius: 5
	    },
	]
    };

    new Chart(ctx, {
	type: 'line',
	data: data,
	options: config
    });

}

function init(query_url, static_url, node, period) {
    // get nodes
    $.get(query_url + "/nodes").done(function(nodes) {
	// title
	$("#node").html(nodes[0]);

	// node selector
	$("#select-node").empty();
	
	nodes.forEach(function(nd) {
	    $("#select-node").append(
		"<label class=\"btn btn-secondary\">\n" + 
	            "<input type=\"radio\" name=\"options\" id=\"" + nd + "\" autocomplete=\"off\" onclick=\"init('" + query_url + "', '" + query_url + "/static/', '" + nd + "', 0);\"> " + nd + "\n" + 
	         "</label>"
	    );
	});
    });

    
    // see if there are already created charts and destroy them if true
    Chart.helpers.each(Chart.instances, function(instance){
	instance.destroy();
    });
    
    $.get(query_url + "/data/" + node + "/soil/" + period).done(function(dataset) {
	gen_chart(dataset, 'soilChart', '🌱 Humedad del suelo', ' %', 100, 0, '#40cc40'); 
    });

    $.get(query_url+"/data/" + node + "/temp/" + period).done(function(dataset) {
       	gen_chart(dataset, 'tempChart', '🌡️ Temperatura', ' ºC', 40, -10, '#cc4040'); 
    });

    $.get(query_url + "/data/" + node + "/hum/" + period).done(function(dataset) {
	gen_chart(dataset, 'humChart', '💧 Humedad del aire', ' %', 100, 0, '#4040cc'); 
    });
    
    $.get(query_url + "/data/" + node + "/baro/" + period).done(function(dataset) {
	gen_chart(dataset, 'baroChart', '🌦️ Presión atmosférica', ' mBar', 1050, 950, '#cc40cc'); 
    });
    
    $.get(query_url + "/data/" + node + "/batt/" + period).done(function(dataset) {
	gen_chart(dataset, 'battChart', '🔋 Voltaje de la batería', ' V', 4.5, 2.5, '#cccc40'); 
    });

}

