package main

import (
	"net/http"
	"fmt"
)


func main() {
	// load config
	config := GetConfig()

	// create routing table
	r := MakeRouter()
	// serve static files
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	// start server
	fmt.Printf("Iniciando server en puerto: %s\n", config.Port);
	http.ListenAndServe("0.0.0.0:" + config.Port, r)
}

