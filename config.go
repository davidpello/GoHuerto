
package main

import (
	"github.com/tkanos/gonfig"
)

const configFile = "config.json"

type Config struct {
	Hostname string
	Port string
	URL string
	AuthKey string
	MongoDBUri string
	MongoDB string
}


func GetConfig() Config {
	config := Config {}

	err := gonfig.GetConf(configFile, &config)
	if err != nil {
		panic(err)
	}
	
	return config
}
