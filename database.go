package main

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
	"fmt"
)

type DataPoint struct {
	Id float64 `bson:"id" json:"id"`
	Baro float64 `bson:"baro" json:"baro"`
	Batt float64 `bson:"batt" json:"batt"`
	Date string `bson:"date" json:"date"`
	Hum float64 `bson:"hum" json:"hum"`
	Soil float64 `bson:"soil" json:"soil"`
	Temp float64 `bson:"temp" json:"temp"`
}

type DataList struct {
	Id float64 `json:"id"`
	Date string `json:"date"`
	Data float64 `json:"data"`
}

// returns a list of the collections in the database, as
// each collection contais the data of just one node
func GetNodeList() []string {

	// connect to the database
	config := GetConfig()
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(config.MongoDBUri))
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	// get collections
	colls, err := client.Database(config.MongoDB).ListCollectionNames(
		context.TODO(),
		bson.D{},
	)
	if err != nil {
		panic(err)
	}

	return colls
	
}

// get last entry
func GetLastDB(node string) DataPoint {
	config := GetConfig()

	// connect to DB
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(config.MongoDBUri))

	if err != nil {
		panic(err)
	}

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	coll := client.Database(config.MongoDB).Collection(node)

	// Get last
	opts := options.Find().SetSort(bson.D{{"_id", -1}}).SetLimit(1)
	cursor, err := coll.Find(context.TODO(), bson.D{}, opts)

	if err != nil {
		panic(err)
	}

	// decode into custom struct
	var results []DataPoint
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err)
	}

	return results[0]
}

// get all data from a node for a period of time
func GetDataList(node string, field string, period int) []DataList {
	config := GetConfig()

	// get timestamp from "period" ago
	currDate := time.Now()

	subDate := currDate.AddDate(0, 0, -1 * 7 * period)
	// if period = 0 return just one day
	if period == 0 {
		subDate = currDate.AddDate(0, 0, -1)
	}

	searchTimestamp := subDate.Unix()
	
	// connect to DB
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(config.MongoDBUri))

	if err != nil {
		panic(err)
	}

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	coll := client.Database(config.MongoDB).Collection(node)

	// Get data
	cursor, err := coll.Find(context.TODO(), bson.M{"id" : bson.M{ "$gt": searchTimestamp }})

	if err != nil {
		panic(err)
	}

	// decode results
	var results []DataPoint
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err)
	}

	var data []DataList
	for _, value := range results {
		var d float64
		switch field {
		    case "baro": d = value.Baro
		    case "temp": d = value.Temp
		    case "soil": d = value.Soil
		    case "hum" : d = value.Hum
		    case "batt": d = value.Batt
		}
		data = append(data, DataList{value.Id, value.Date, d})
	}

	return data
}

func InsertData(data DataPoint, node string) {
	
	fmt.Printf("Node: %s => temp: %f, baro: %f\n", node, data.Temp, data.Baro)

	config := GetConfig()

	// connect
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(config.MongoDBUri))
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	// select collection
	coll := client.Database(config.MongoDB).Collection(node)

	// insert data
	currDate := time.Now()
	mongoData := bson.D{
		{"id", currDate.Unix()},
		{"date", currDate.Format(time.RFC3339)},
		{"baro", data.Baro},
		{"batt", data.Batt},
		{"soil", data.Soil},
		{"temp", data.Temp},
		{"hum", data.Hum}}
	_, err = coll.InsertOne(context.TODO(), mongoData)
	if err != nil {
		panic(err)
	}
}
