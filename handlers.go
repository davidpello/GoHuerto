package main

import (
	"net/http"
	"fmt"
	"github.com/gorilla/mux"
	"encoding/json"
	"strconv"
	"io/ioutil"
	"html/template"
)


func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Ola mundo ke ase\n")
}


func UploadHandler(w http.ResponseWriter, r *http.Request) {
	config := GetConfig()
	
	if r.Header.Get("X-Shitty-Auth") != config.AuthKey {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "Unauthorized\n")
		return
	}

	// decode json
	var jsonmap map[string]interface{}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(body, &jsonmap)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Bad JSON data\n")
		fmt.Fprintf(w, err.Error())
		return
	}

	// extract data
	msg := jsonmap["uplink_message"].(map[string]interface{})
	device := jsonmap["end_device_ids"].(map[string]interface{})
	node := device["device_id"].(string)
	payload := msg["decoded_payload"].(map[string]interface{})
	
	soil := payload["analog_in_0"].(float64)
	batt := payload["analog_in_1"].(float64)
	baro := payload["barometric_pressure_0"].(float64)
	hum := payload["relative_humidity_0"].(float64)
	temp := payload["temperature_0"].(float64)



	var datapoint = DataPoint {
		Id : 0,
		Baro: baro,
		Batt: batt,
		Date: "",
		Hum: hum,
		Soil: soil,
		Temp: temp,
	}
	
	// Insert it
	InsertData(datapoint, node)
	
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Upload OK\n")
}

func DataHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	p, err := strconv.Atoi(vars["period"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Bad request")
		return
	}
	data := GetDataList(vars["node"], vars["field"], p)
	j, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Problem with the data")
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, string(j))
}

func LastHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	node := vars["node"]
	
	tmpl := template.Must(template.ParseFiles("templates/base.html",
		"templates/last.html",
	))
	data := struct {
		Title string
		Data DataPoint
	} {
		Title: "Último dato",
		Data: GetLastDB(node),
	}
	tmpl.ExecuteTemplate(w, "base", data)
}

func NodesHandler(w http.ResponseWriter, r *http.Request) {
	
	nodes := GetNodeList()

	j, err := json.Marshal(nodes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "problem retrieving collections from database")
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, string(j))
}

func ChartsHandler(w http.ResponseWriter, r *http.Request) {
	// read configuration
	config := GetConfig()
	
	tmpl := template.Must(template.ParseFiles("templates/charts.html"))
	data := struct {
		Title string
		URL string
	} {
		Title: "Charts",
		URL: config.URL,
	}
	tmpl.ExecuteTemplate(w, "charts", data)
}
